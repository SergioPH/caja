const con = require('./db');
var Caja = function(params){
    this.idColaboradores = params.idColaboradores;
    this.Contrato = params.Contrato;
    this.Usuarios_idUsuarios = params.Usuarios_idUsuarios;
    this.Secciones_idSecciones = params.Secciones_idSecciones;
    //this.idAbonos = params.idAbonos1;
    this.Pagos_idPagos = params.Pagos_idPagos1;
    this.Colaboradores_idColaboradores = params.Colaboradores_idColaboradores1;
    this.Alumnos_Matricula = params.Alumnos_Matricula1;
    this.FechaPago = params.FechaPago1;
    this.Monto = params.Monto1;
    this.MetodoPago = params.MetodoPago1;
    this.Prorrogas_idProrrogas = params.Prorrogas_idProrrogas1;
    //Convenios
    this.idConvenios = params.idConvenios1;
    this.Descripcion = params.Descripcion1;
    this.FechaFin = params.FechaFin1;
    this.FechaPago = params.FechaPago1;
    this.Alumnos_Matricula = params.Alumnos_Matricula1;
    this.TipodePago_idTipodePago = params.TipodePago_idTipodePago1;
    //AlumnoBeca
    this.Matricula = params.Matricula;
    this.PorcentajeBecaTrabajo = params.PorcentajeBecaTrabajo;
    this.PorcentajeBecaSEP = params.PorcentajeBecaSEP;
    //Bloqueos
    this.Status = params.Status;
    //
    this.Fecha = params.Fecha;
    this.Abonos_idAbonos = params.Abonos_idAbonos;
    this.MesesAPagar = params.MesesAPagar;
    this.DescuentoPorMes = params.DescuentoPorMes;
    this.CiclosEscolares_idCiclosEscolares = params.CiclosEscolares_idCiclosEscolares;
}
Caja.getPagos = function(id,result){

    con.query(
        'SELECT * FROM Abonos',id,function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
Caja.getBecados = function(id,result){

    con.query(
        'SELECT Usuarios.Nombre,Usuarios.Apellido_P,Usuarios.Apellido_M,Secciones.Seccion,Grados.Grado,Grupos.Grupo,Alumnos.PorcentajeBecaSEP,Alumnos.PorcentajeBecaTrabajo FROM `Alumnos` INNER JOIN Usuarios ON Alumnos.Usuarios_idUsuarios=Usuarios.idUsuarios INNER JOIN Grupos ON Alumnos.Grupos_idGrupos=Grupos.idGrupos INNER JOIN Grados ON Grupos.Grados_idGrados=Grados.idGrados INNER JOIN Secciones ON Grados.Secciones_idSecciones=Secciones.idSecciones WHERE Alumnos.PorcentajeBecaTrabajo AND Alumnos.PorcentajeBecaSEP is not null',id,function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
Caja.getProrrogas = function(id,result){

    con.query(
        'SELECT Alumnos_Prorrogas.FechaFin,Usuarios.Nombre As NombreAlumno,Usuarios.Apellido_P,Usuarios.Apellido_M,TipodePago.Nombre As AplicaPara,Alumnos_Prorrogas.Completada FROM `Alumnos_Prorrogas` INNER JOIN Alumnos ON Alumnos_Prorrogas.Alumnos_Matricula=Alumnos.Matricula INNER JOIN Usuarios ON Alumnos.Usuarios_idUsuarios=Usuarios.idUsuarios INNER JOIN Pagos ON Alumnos_Prorrogas.Pagos_idPagos=Pagos.idPagos INNER JOIN TipodePago ON Pagos.TipodePago_idTipodePago=TipodePago.idTipodePago',id,function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
Caja.getSalidas = function(id,result){

    con.query(
        'SELECT * FROM Salidas INNER JOIN Pagos ON Salidas.Pagos_idPagos=Pagos.idPagos INNER JOIN TipodePago ON Pagos.TipodePago_idTipodePago=TipodePago.idTipodePago',id,function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
Caja.postColegiaturas = function(id,result){
console.log(id.Pagos_idPagos)
    con.query(
        'INSERT INTO Abonos (`Pagos_idPagos`, `Colaboradores_idColaboradores`, `FechaPago`, `Monto`, `Alumnos_Matricula`, `MetodoPago`, `Prorrogas_idProrrogas`) VALUES('
        +id.Pagos_idPagos+','
        +id.Colaboradores_idColaboradores+',"'
        +id.FechaPago+'",'
        +id.Monto+','
        +id.Alumnos_Matricula+',"'
        +id.MetodoPago+'",'
        +id.Prorrogas_idProrrogas+')',function(error,resp){
            if(error)
            {
                result(error,null)
            }
            else {
                result(null,resp)
            }
        } 
    )
}
Caja.postConvenios = function(id,result){
        con.query(
            'INSERT INTO `Convenios`(`idConvenios`, `Descripcion`, `FechaFin`, `FechaPago`, `Alumnos_Matricula`, `TipodePago_idTipodePago`) VALUES("'
            +id.idConvenios+'","'
            +id.Descripcion+'","'
            +id.FechaFin+'","'
            +id.FechaPago+'",'
            +id.Alumnos_Matricula+','
            +id.TipodePago_idTipodePago+')',function(error,resp){
                if(error)
                {
                    result(error,null)
                }
                else {
                    result(null,resp)
                }
            } 
        )
    }
    Caja.getConvenios = function(id,result){

        con.query(
            'SELECT Convenios.idConvenios,Usuarios.Nombre,Usuarios.Apellido_P,Usuarios.Apellido_M,Convenios.Descripcion,Convenios.FechaPago FROM `Convenios` INNER JOIN Alumnos ON Convenios.Alumnos_Matricula=Alumnos.Matricula INNER JOIN Usuarios ON Alumnos.Usuarios_idUsuarios=Usuarios.idUsuarios',id,function(error,resp){
                if(error)
                {
                    result(error,null)
                }
                else {
                    result(null,resp)
                }
            } 
        )
    }
    Caja.postBecas = function(id,result){
        con.query(
            'UPDATE `Alumnos` SET `PorcentajeBecaSEP` = '+id.PorcentajeBecaSEP+', `PorcentajeBecaTrabajo` = '+id.PorcentajeBecaTrabajo+' WHERE CONCAT(`Alumnos`.`Matricula`) = '+id.Matricula+'',function(error,resp){
                if(error)
                {
                    result(error,null)
                }
                else {
                    result(null,resp)
                }
            } 
        )
    }
    Caja.postProrrogas = function(id,result){
        con.query(
            'INSERT INTO `Alumnos_Prorrogas`(`Alumnos_Matricula`, `Pagos_idPagos`, `FechaFin`) VALUES ('+id.Alumnos_Matricula+','+id.Pagos_idPagos+',"'+id.FechaFin+'")',function(error,resp){
                if(error)
                {
                    result(error,null)
                }
                else {
                    result(null,resp)
                }
            } 
        )
    }
    Caja.postBloqueos = function(id,result){
        con.query(
            'INSERT INTO `Bloqueos`(`Alumnos_Matricula`, `Pagos_idPagos`, `Status`) VALUES ('+id.Alumnos_Matricula+','+id.Pagos_idPagos+','+id.Status+')',function(error,resp){
                if(error)
                {
                    result(error,null)
                }
                else {
                    result(null,resp)
                }
            } 
        )
    }
    Caja.postFacturas = function(id,result){
        con.query(
            'INSERT INTO `Facturas`(`Colaboradores_idColaboradores`, `Fecha`, `Abonos_idAbonos`, `Alumnos_Matricula`) VALUES ('+id.Colaboradores_idColaboradores+',"'+id.Fecha+'",'+id.Abonos_idAbonos+','+id.Alumnos_Matricula+')',function(error,resp){
                if(error)
                {
                    result(error,null)
                }
                else {
                    result(null,resp)
                }
            } 
        )
    }
    Caja.getFacturas = function(id,result){
        con.query(
            'SELECT UA.Nombre,UA.Apellido_P,UA.Apellido_M,Secciones.idSecciones,Fecha,Abonos.FechaPago,Estado FROM Facturas INNER JOIN Alumnos ON Facturas.Alumnos_Matricula = Alumnos.Matricula INNER JOIN Usuarios UA ON Alumnos.Usuarios_idUsuarios=UA.idUsuarios INNER JOIN Grupos ON Alumnos.Grupos_idGrupos=Grupos.idGrupos INNER JOIN Grados ON Grupos.Grados_idGrados=Grados.idGrados INNER JOIN Secciones ON Grados.Secciones_idSecciones=Secciones.idSecciones INNER JOIN Abonos ON Facturas.Abonos_idAbonos=Abonos.idAbonos',id,function(error,resp){
                if(error)
                {
                    result(error,null)
                }
                else {
                    result(null,resp)
                }
            } 
        )
    }
    Caja.postPagos = function(id,result){
        con.query(
            'INSERT INTO `Pagos`(`TipodePago_idTipodePago`, `FechaFin`, `Monto`, `MesesAPagar`, `DescuentoPorMes`, `CiclosEscolares_idCiclosEscolares`) VALUES ('+id.TipodePago_idTipodePago+',"'+id.FechaFin+'",'+id.Monto+','+id.MesesAPagar+','+id.DescuentoPorMes+','+id.CiclosEscolares_idCiclosEscolares+')',function(error,resp){
                if(error)
                {
                    result(error,null)
                }
                else {
                    result(null,resp)
                }
            } 
        )
    }
    Caja.getPagos = function(id,result){
        con.query(
            'SELECT CiclosEscolares.CiclosEscolar,Secciones.Seccion,TipodePago.Nombre,Pagos.MesesAPagar,Monto FROM `Pagos` INNER JOIN CiclosEscolares ON Pagos.CiclosEscolares_idCiclosEscolares=CiclosEscolares.idCiclosEscolares INNER JOIN Secciones ON CiclosEscolares.Secciones_idSecciones=Secciones.idSecciones INNER JOIN TipodePago ON Pagos.TipodePago_idTipodePago=TipodePago.idTipodePago',id,function(error,resp){
                if(error)
                {
                    result(error,null)
                }
                else {
                    result(null,resp)
                }
            } 
        )
    }
module.exports = Caja;