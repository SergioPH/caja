module.exports = function(app){
    const Caja = require('../Controller/Caja')
    //Caja
    app.route('/InicioCaja').get(Caja.getPagos);
    app.route('/Becados').get(Caja.getBecados);
    app.route('/Prorrogas').get(Caja.getProrrogas);
    app.route('/Salidas').get(Caja.getSalidas);
    app.route('/Entradas').post(Caja.postColegiaturas);
    app.route('/Convenios').post(Caja.postConvenios);
    app.route('/ListaConvenios').get(Caja.getConvenios);
    app.route('/Becar').post(Caja.postBecas);
    app.route('/AsignarProrroga').post(Caja.postProrrogas);
    app.route('/Bloqueos').post(Caja.postBloqueos);
    app.route('/RegistraFactura').post(Caja.postFacturas);
    app.route('/Facturas').get(Caja.getFacturas);
    app.route('/Pagos').post(Caja.postPagos);
    app.route('/ListaPagos').get(Caja.getPagos);
}