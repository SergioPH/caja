const CajaModel = require('../Model/Caja');
exports.getPagos = (req,res) =>{
    CajaModel.getPagos(req.params.id,function(error,id){
        if(error)
            res.send(error)
        res.send(id)
    })
}
exports.getBecados = (req,res) =>{
    CajaModel.getBecados(req.params.id,function(error,id){
        if(error)
            res.send(error)
        res.send(id)
    })
}
exports.getProrrogas = (req,res) =>{
    CajaModel.getProrrogas(req.params.id,function(error,id){
        if(error)
            res.send(error)
        res.send(id)
    })
}
exports.getSalidas = (req,res) =>{
    CajaModel.getSalidas(req.params.id,function(error,id){
        if(error)
            res.send(error)
        res.send(id)
    })
}
exports.postColegiaturas = (req,res) =>{
    var modelS = new CajaModel(req.body)
    if(modelS == ''){
        res.status(400).send({error: true, message:'Datos no validos'})
    }
    else{
        CajaModel.postColegiaturas(modelS,function(error,modelS){
            if(error)
                res.send(error)
            res.send(modelS)
        })   
    }
}
exports.postConvenios = (req,res) =>{
    var modelC = new CajaModel(req.body)
    if(modelC == ''){
        res.status(400).send({error: true, message:'Datos no validos'})
    }
    else{
        CajaModel.postConvenios(modelC,function(error,modelC){
            if(error)
                res.send(error)
            res.send(modelC)
        })   
    }
}
exports.getConvenios = (req,res) =>{
    CajaModel.getConvenios(req.params.id,function(error,id){
        if(error)
            res.send(error)
        res.send(id)
    })
}
exports.postBecas = (req,res) =>{
    var modelD = new CajaModel(req.body)
    if(modelD == ''){
        res.status(400).send({error: true, message:'Datos no validos'})
    }
    else{
        CajaModel.postBecas(modelD,function(error,modelD){
            if(error)
                res.send(error)
            res.send(modelD)
        })   
    }
}
exports.postProrrogas = (req,res) =>{
    var modelE = new CajaModel(req.body)
    if(modelE == ''){
        res.status(400).send({error: true, message:'Datos no validos'})
    }
    else{
        CajaModel.postProrrogas(modelE,function(error,modelE){
            if(error)
                res.send(error)
            res.send(modelE)
        })   
    }
}
exports.postBloqueos = (req,res) =>{
    var modelF = new CajaModel(req.body)
    if(modelF == ''){
        res.status(400).send({error: true, message:'Datos no validos'})
    }
    else{
        CajaModel.postBloqueos(modelF,function(error,modelF){
            if(error)
                res.send(error)
            res.send(modelF)
        })   
    }
}
exports.postFacturas = (req,res) =>{
    var modelG = new CajaModel(req.body)
    if(modelG == ''){
        res.status(400).send({error: true, message:'Datos no validos'})
    }
    else{
        CajaModel.postFacturas(modelG,function(error,modelG){
            if(error)
                res.send(error)
            res.send(modelG)
        })   
    }
}
exports.getFacturas = (req,res) =>{
    CajaModel.getFacturas(req.params.id,function(error,id){
        if(error)
            res.send(error)
        res.send(id)
    })
}
exports.postPagos = (req,res) =>{
    var modelG = new CajaModel(req.body)
    if(modelG == ''){
        res.status(400).send({error: true, message:'Datos no validos'})
    }
    else{
        CajaModel.postPagos(modelG,function(error,modelG){
            if(error)
                res.send(error)
            res.send(modelG)
        })   
    }
}
exports.getPagos = (req,res) =>{
    CajaModel.getPagos(req.params.id,function(error,id){
        if(error)
            res.send(error)
        res.send(id)
    })
}